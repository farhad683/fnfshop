package com.farhad.fnfshop.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.farhad.fnfshop.ui.product.DescriptionFragment;
import com.farhad.fnfshop.ui.product.MoreInfoFragment;
import com.farhad.fnfshop.ui.product.ReviewsFragment;

public class ProductDescTabsAdapter extends FragmentStatePagerAdapter {
    int id;
    public ProductDescTabsAdapter(FragmentManager fm, int id){
        super(fm);
        this.id = id;
    }
    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Description";
            case 1:
                return "More Info";
            case 2:
                return "Reviews";
            default:
                return null;
        }
    }

    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                return new DescriptionFragment(id);
            case 1:
                return new MoreInfoFragment(id);
            case 2:
                return new ReviewsFragment(id);
            default:
                return null;
        }
    }
}
