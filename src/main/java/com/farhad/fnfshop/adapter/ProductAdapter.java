package com.farhad.fnfshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.fnfshop.R;
import com.farhad.fnfshop.ui.ProductActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import me.gilo.woodroid.models.Product;

public class ProductAdapter  extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
    private List<Product> mProductList;
    private Context mContext;

    public static class ProductViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImageView;
        public TextView mTextView1, mTextView2;
        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.image_view_product);
            mTextView1 = itemView.findViewById(R.id.text_view_title);
            mTextView2 = itemView.findViewById(R.id.text_view_price);
        }
    }
    public ProductAdapter(Context context, List<Product> exampleItemArrayList){
        mContext = context;
        mProductList = exampleItemArrayList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item, parent, false);
        ProductViewHolder evh = new ProductViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int position) {
        Product currentItem = mProductList.get(position);
//        holder.mImageView.setImageResource(currentItem.getImageResource());

        Picasso.get().load(currentItem.getImages().get(0).getSrc())
                .fit().centerInside().into(holder.mImageView);
        holder.mTextView1.setText(currentItem.getTitle());
        holder.mTextView2.setText("Tk. " + currentItem.getPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductActivity.class);
                intent.putExtra("PRODUCT_ID_TAG", currentItem.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}
