package com.farhad.fnfshop.models;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.farhad.fnfshop.api.ApiClient;
import com.farhad.fnfshop.api.ApiServices;

import java.util.List;

import me.gilo.woodroid.models.Product;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductRepository {
    private ApiServices apiServices;
    private Application application;
    private MutableLiveData<List<Product>> allProductsData;
    private MutableLiveData<Product> productData;

    public ProductRepository(Application application) {
        this.application = application;
        apiServices = ApiClient.getInstance().create(ApiServices.class);
        allProductsData = new MutableLiveData<>();
        productData = new MutableLiveData<>();

    }

    public LiveData<List<Product>> getAllProducts(){
        apiServices.getProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if(response.isSuccessful()){
                    allProductsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {

            }
        });

        return allProductsData;
    }

    public MutableLiveData<Product> getProductData(int id) {
        apiServices.getProduct(id).enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                if (response.isSuccessful()){
                    productData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });

        return productData;
    }
}
