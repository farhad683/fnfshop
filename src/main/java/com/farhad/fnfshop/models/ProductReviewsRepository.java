package com.farhad.fnfshop.models;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.farhad.fnfshop.api.ApiClient;
import com.farhad.fnfshop.api.ApiServices;

import java.util.List;

import me.gilo.woodroid.models.ProductReview;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductReviewsRepository {
    private ApiServices apiServices;
    private Application application;
    private MutableLiveData<List<ProductReview>> allReviewsData;
    private MutableLiveData<ProductReview> reviewData;

    public ProductReviewsRepository(Application application) {
        this.apiServices = ApiClient.getInstance().create(ApiServices.class);
        this.application = application;
        this.allReviewsData = new MutableLiveData<>();
        this.reviewData = new MutableLiveData<>();
    }

    public MutableLiveData<ProductReview> getReview(int id){
        apiServices.getReview(id).enqueue(new Callback<ProductReview>() {
            @Override
            public void onResponse(Call<ProductReview> call, Response<ProductReview> response) {
                if(response.isSuccessful()){
                    reviewData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductReview> call, Throwable t) {

            }
        });

        return reviewData;
    }

    public MutableLiveData<List<ProductReview>> getAllReviews(){
        apiServices.getReviews().enqueue(new Callback<List<ProductReview>>() {
            @Override
            public void onResponse(Call<List<ProductReview>> call, Response<List<ProductReview>> response) {
                if(response.isSuccessful()){
                    allReviewsData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<ProductReview>> call, Throwable t) {

            }
        });
        return allReviewsData;
    }
}
