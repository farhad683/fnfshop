package com.farhad.fnfshop.models;

import android.app.Application;

import androidx.lifecycle.MutableLiveData;

import com.farhad.fnfshop.Config;
import com.farhad.fnfshop.api.ApiClient;
import com.farhad.fnfshop.api.ApiServices;

import java.util.List;

import me.gilo.woodroid.models.Category;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryRepository {
    private Application application;
    private ApiServices apiServices;

    private MutableLiveData<List<Category>> allCategoryData;
    private MutableLiveData<Category> categoryData;

    private me.gilo.woodroid.repo.product.CategoryRepository categoryRepo;

    public CategoryRepository(Application application) {
        this.application = application;
        this.apiServices = ApiClient.getInstance().create(ApiServices.class);
        this.allCategoryData = new MutableLiveData<>();

        categoryRepo = new me.gilo.woodroid.repo.product.CategoryRepository(Config.BASE_URL, Config.CONSUMER_KEY, Config.CONSUMER_SECRET);
    }

    public MutableLiveData<List<Category>> getAllCategoryData(){
        apiServices.getCategories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if(response.isSuccessful()){
                    allCategoryData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });

        return allCategoryData;
    }

    public MutableLiveData<Category> getCategoryData(int id) {
        categoryRepo.category(id).enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {

            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {

            }
        });
        return categoryData;
    }

    public MutableLiveData<List<Category>> getCats(){
        categoryRepo.categories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()){
                    allCategoryData.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {

            }
        });
        return allCategoryData;
    }
}
