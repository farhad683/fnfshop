package com.farhad.fnfshop.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.fnfshop.models.CategoryRepository;

import java.util.List;

import me.gilo.woodroid.models.Category;

public class CategoryViewModel extends AndroidViewModel {

    private CategoryRepository categoryRepository;
    private me.gilo.woodroid.repo.product.CategoryRepository catRepo;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
        categoryRepository = new CategoryRepository(application);
    }

    public LiveData<List<Category>> getAllCategory(){
        return categoryRepository.getAllCategoryData();
    }

    public LiveData<Category> getCategory(int id){
        return categoryRepository.getCategoryData(id);
    }
}
