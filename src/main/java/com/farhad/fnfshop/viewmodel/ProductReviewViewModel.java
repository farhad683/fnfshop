package com.farhad.fnfshop.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.fnfshop.models.ProductReviewsRepository;

import java.util.List;

import me.gilo.woodroid.models.ProductReview;

public class ProductReviewViewModel extends AndroidViewModel {
    private ProductReviewsRepository productReviewsRepository;

    public ProductReviewViewModel(@NonNull Application application) {
        super(application);
        this.productReviewsRepository = new ProductReviewsRepository(application);
    }

    public LiveData<ProductReview> getReview(int id){
        return productReviewsRepository.getReview(id);
    }

    public LiveData<List<ProductReview>> getAllReviews(){
        return productReviewsRepository.getAllReviews();
    }
}
