package com.farhad.fnfshop.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.farhad.fnfshop.models.ProductRepository;

import java.util.List;

import me.gilo.woodroid.models.Product;

public class ProductViewModel extends AndroidViewModel {
    
    private ProductRepository productRepository;

    public ProductViewModel(@NonNull Application application) {
        super(application);
        productRepository = new ProductRepository(application);
    }

    public LiveData<List<Product>> getAllProducts(){
        return productRepository.getAllProducts();
    }

    public LiveData<Product> getProduct(int id){
        return productRepository.getProductData(id);
    }

}
