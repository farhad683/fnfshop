package com.farhad.fnfshop.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.farhad.fnfshop.R;
import com.farhad.fnfshop.adapter.ProductDescTabsAdapter;
import com.farhad.fnfshop.viewmodel.ProductViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import me.gilo.woodroid.models.Product;
import me.relex.circleindicator.CircleIndicator;

public class ProductActivity extends AppCompatActivity {
    private final String PRODUCT_ID_TAG = "PRODUCT_ID_TAG";
    private int productId;

    private ProductViewModel productViewModel;

    // views
    private ViewPager descriptionTabViewPager;
    private TabLayout productTabLayout;
    private FrameLayout flLoading;
    private ViewPager productViewPagerImage;
    private CircleIndicator circleIndicator;
    private TextView tvCallToAction, tvOnSale, product_acitvity_review_count, product_activity_price;
    private TextView tvTitle, tvShortDescripion;
    private FloatingActionButton floatingActionButton;
    private AppCompatRatingBar product_activity_reviews;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        viewInitializer();

        productId = getIntent().getIntExtra(PRODUCT_ID_TAG, 0);


        descriptionTabViewPager.setAdapter(new ProductDescTabsAdapter(getSupportFragmentManager(), productId));
        productTabLayout.setupWithViewPager(descriptionTabViewPager, false);


        productViewModel.getProduct(productId).observe(this, new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                setUpPageData(product);
                flLoading.setVisibility(View.GONE);

                toolbar.setTitle(product.getTitle());
            }
        });

    }

    private void viewInitializer() {
        productTabLayout = findViewById(R.id.product_activity_tab_layout);
        flLoading = findViewById(R.id.flLoading);
        descriptionTabViewPager = findViewById(R.id.view_pager_details);
        productViewPagerImage = findViewById(R.id.vpImages);
        circleIndicator = findViewById(R.id.indicator);
        tvCallToAction = findViewById(R.id.tvCallToAction);
        tvOnSale = findViewById(R.id.tvOnSale);
        floatingActionButton = findViewById(R.id.fab);

        tvTitle = findViewById(R.id.tvTitle);
        product_activity_reviews = findViewById(R.id.product_activity_reviews);
        product_acitvity_review_count = findViewById(R.id.product_acitvity_review_count);
        product_activity_price = findViewById(R.id.product_activity_price);
        tvShortDescripion = findViewById(R.id.product_activity_tv_short_description);


    }

    private void setUpPageData(Product product) {
        setTitle(product.getTitle());
        tvTitle.setText(product.getTitle());
        product_activity_reviews.setRating(Float.parseFloat(product.getAverage_rating()));
        product_acitvity_review_count.setText(product.getRating_count()+"");
        product_activity_price.setText("\u09F3 "+product.getPrice()+"");
        tvShortDescripion.setText(HtmlCompat.fromHtml(product.getShort_description().trim(), 0));

        if(product.getImages() != null && !product.getImages().isEmpty()){
            productViewPagerImage.setOffscreenPageLimit(product.getImages().size());
            productViewPagerImage.setAdapter(new ImagePagerAdapter(this, product.getImages()));
            circleIndicator.setViewPager(productViewPagerImage);
        }

        if(product.isOn_sale()){
            tvCallToAction.setText(HtmlCompat.fromHtml(product.getPrice_html().trim(), 0));
            tvOnSale.setVisibility(View.VISIBLE);
        } else {
            tvCallToAction.setText(HtmlCompat.fromHtml(product.getPrice_html().trim(), 0));
            tvOnSale.setVisibility(View.GONE);
        }

        // Cart code will go here

    }
}
