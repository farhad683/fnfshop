package com.farhad.fnfshop.ui.home;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterViewFlipper;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.farhad.fnfshop.R;
import com.farhad.fnfshop.adapter.CategoryAdapter;
import com.farhad.fnfshop.adapter.ProductAdapter;
import com.farhad.fnfshop.adapter.SliderAdapter;
import com.farhad.fnfshop.api.ApiClient;
import com.farhad.fnfshop.api.ApiServices;
import com.farhad.fnfshop.models.SliderItem;
import com.farhad.fnfshop.viewmodel.CategoryViewModel;
import com.farhad.fnfshop.viewmodel.ProductViewModel;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import me.gilo.woodroid.models.Category;
import me.gilo.woodroid.models.Product;

public class HomeFragment extends Fragment {
    private static final String TAG = "TAG";
    private ApiServices apiServices;

    private ProductViewModel productViewModel;
    private CategoryViewModel categoryViewModel;

    private HomeViewModel homeViewModel;

    private AdapterViewFlipper simpleAdapterViewFlipper;
    int[] fruitImages = {R.drawable.banner2, R.drawable.banner2, R.drawable.banner2};     // array of images
    String fruitNames[] = {"Top Brands Phone", "Top Brands Watch", "Top Brands Glasses"};

    private RecyclerView mCatRecyclerView;
    private RecyclerView.Adapter mCategoryAdapter;
    private RecyclerView.LayoutManager mCatLayoutManager;
    private List<Category> mCategoryList;

    private RecyclerView mProdRecyclerView;
    private RecyclerView.Adapter mProdAdapter;
    private RecyclerView.LayoutManager mProdLayoutManager;
    private List<Product> mProductList;


    private SliderView sliderView;
    private SliderAdapter adapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        productViewModel = new ViewModelProvider(this).get(ProductViewModel.class);
        categoryViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);

        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.INTERNET},1);
        }
        apiServices= ApiClient.getInstance().create(ApiServices.class);
        initializeSlider(root);
        initializeProducts(root);
        mProductList = new ArrayList<>();


        buildCategoryRecyclerView(root);
        createCategoryList();


//        buildProductRecyclerView(root);
        createProductList(root);

        Log.d(TAG, "onCreateView: " + mProductList.size());
        return root;

    }

    private void buildCategoryRecyclerView(View root) {
        mCatRecyclerView = root.findViewById(R.id.category_recyler);
        mCatRecyclerView.setHasFixedSize(true);
        mCatLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        mCatRecyclerView.setLayoutManager(mCatLayoutManager);
//        mCatRecyclerView.setAdapter(mCategoryAdapter);
    }

    private void createCategoryList() {
        mCategoryList = new ArrayList<>();

        categoryViewModel.getAllCategory().observe(getActivity(), new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                for(Category category: categories){
                    if(category.getImage() != null){
                        mCategoryList.add(category);
                    }
                }
                mCategoryAdapter = new CategoryAdapter(getContext(), mCategoryList);
                mCatRecyclerView.setAdapter(mCategoryAdapter);
            }
        });



    }

    private void buildProductRecyclerView(View root, List<Product> products) {
        mProdRecyclerView = root.findViewById(R.id.product_recyler);
        mProdRecyclerView.setHasFixedSize(true);
        mProdLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        mProdAdapter = new ProductAdapter(getContext(), products);
        mProdRecyclerView.setAdapter(mProdAdapter);
        mProdRecyclerView.setLayoutManager(mProdLayoutManager);

    }

    private void createProductList(View root) {

        productViewModel.getAllProducts().observe(getActivity(), new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                buildProductRecyclerView(root, products);
                Toast.makeText(getContext(), "Size: " + products.size(), Toast.LENGTH_SHORT).show();
            }
        });

        /*apiServices.getProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if(response.isSuccessful()){
                    mProductList = response.body();
                    mProdAdapter = new ProductAdapter(getContext(), mProductList);
                    mProdRecyclerView.setAdapter(mProdAdapter);
                } else {

                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {

            }
        });*/
    }

    private void initializeProducts(View root) {
    }

    private void initializeSlider(View root) {
        sliderView = root.findViewById(R.id.imageSlider);

        adapter = new SliderAdapter(getContext());

        sliderView.setSliderAdapter(adapter);
        renewItems();

        sliderView.setIndicatorAnimation(IndicatorAnimations.THIN_WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.RED);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4);
        sliderView.setAutoCycle(true);
    }

    public void renewItems() {
        List<SliderItem> sliderItemList = new ArrayList<>();
        //dummy data
        for (int i = 0; i < 5; i++) {
            SliderItem sliderItem = new SliderItem();
            sliderItem.setDescription("Top Brands Phone " + i);
            if (i % 2 == 0) {
                sliderItem.setImageUrl("https://www.fnf-shop.com/wp-content/uploads/2020/04/Rectangle-7.png");
//                sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
            } else {
                sliderItem.setImageUrl("https://www.fnf-shop.com/wp-content/uploads/2019/12/Ad_570X270_01.jpg");
//                sliderItem.setImageUrl("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
            }
            sliderItemList.add(sliderItem);
        }
        adapter.renewItems(sliderItemList);
    }

    public void removeLastItem(View view) {
        if (adapter.getCount() - 1 >= 0)
            adapter.deleteItem(adapter.getCount() - 1);
    }

    public void addNewItem() {
        SliderItem sliderItem = new SliderItem();
        sliderItem.setDescription("Slider Item Added Manually");
        sliderItem.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
        adapter.addItem(sliderItem);
    }

}