package com.farhad.fnfshop.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.farhad.fnfshop.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import me.gilo.woodroid.models.Image;

class ImagePagerAdapter extends PagerAdapter {
    private Context context;
    private List<Image> imageList;
    public ImagePagerAdapter(Context context, List<Image> images) {
        this.context = context;
        imageList = new ArrayList<>();
        imageList = images;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.single_product_image, container, false);

        ImageView ivImage = v.findViewById(R.id.ivImage);

        Image img = imageList.get(position);
        if(img != null){
            Picasso.get().load(img.getSrc()).fit().centerInside().into(ivImage);
        }


        container.addView(v);
        return v;
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
        container.removeView((View) object);
    }
}
