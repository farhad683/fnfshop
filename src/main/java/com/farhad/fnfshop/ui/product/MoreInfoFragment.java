package com.farhad.fnfshop.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.farhad.fnfshop.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreInfoFragment extends Fragment {

    public MoreInfoFragment(int id) {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more_info, container, false);
    }
}
