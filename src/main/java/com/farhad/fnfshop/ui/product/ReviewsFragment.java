package com.farhad.fnfshop.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.farhad.fnfshop.R;
import com.farhad.fnfshop.viewmodel.ProductReviewViewModel;


public class ReviewsFragment extends Fragment {
    private int productId;

    private ProductReviewViewModel productReviewViewModel;

    public ReviewsFragment(int id) {
        // Required empty public constructor
        this.productId = id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_reviews, container, false);
        productReviewViewModel = new ViewModelProvider(getActivity()).get(ProductReviewViewModel.class);
        /*productReviewViewModel.getAllReviews().observe(getActivity(), new Observer<List<ProductReview>>() {
            @Override
            public void onChanged(List<ProductReview> productReviews) {
                TextView tvReviews = root.findViewById(R.id.tvReviews);
                String txtReviews = "";
                for(ProductReview rev: productReviews){
                    txtReviews += rev.getReviewer();
                    txtReviews+= "" + rev.getReview();
                    txtReviews += "\n\n";
                }
                tvReviews.setText(txtReviews);
            }
        });*/

        return root;
    }
}
