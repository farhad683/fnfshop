package com.farhad.fnfshop.ui.product;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.farhad.fnfshop.R;
import com.farhad.fnfshop.viewmodel.ProductViewModel;

import me.gilo.woodroid.models.Product;


public class DescriptionFragment extends Fragment {
    int productId;

    private ProductViewModel productViewModel;


    public DescriptionFragment(int id) {
        // Required empty public constructor
        this.productId = id;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_description, container, false);

        productViewModel = new ViewModelProvider(getActivity()).get(ProductViewModel.class);
        productViewModel.getProduct(productId).observe(getActivity(), new Observer<Product>() {
            @Override
            public void onChanged(Product product) {
                TextView desc = root.findViewById(R.id.tvDescDetails);
                desc.setText(HtmlCompat.fromHtml(product.getDescription().trim(), 0));
            }
        });

        return root;
    }
}
