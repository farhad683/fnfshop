package com.farhad.fnfshop.api;

/*
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;*/

import android.util.Base64;

import com.farhad.fnfshop.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import me.gilo.woodroid.data.auth.AuthIntercepter;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final String AUTH = "Basic "+ Base64.encodeToString((Config.CONSUMER_KEY+":" + Config.CONSUMER_SECRET).getBytes(), Base64.NO_WRAP);

    public static String consumerKey = Config.CONSUMER_KEY;
    public static String consumerSecret = Config.CONSUMER_SECRET;
    public static final String BASE_URL=Config.BASE_URL;
    public static   Retrofit retrofit;

    public static   Retrofit getInstance(){
        if(retrofit != null){
            return retrofit;
        } else {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    .create();


            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new AuthIntercepter(consumerKey, consumerSecret))
                    .addInterceptor(loggingInterceptor)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(15, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
            return retrofit;
        }
    }

}
