package com.farhad.fnfshop.api;

import java.util.List;

import me.gilo.woodroid.models.Category;
import me.gilo.woodroid.models.Product;
import me.gilo.woodroid.models.ProductReview;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ApiServices {
    // Product------------------------------
    @GET("products?per_page=30")
    Call<List<Product>> getProducts();

    @GET("products/{id}")
    Call<Product> getProduct(@Path("id") int id);

    // Category-----------------------------
    @GET("products/categories?per_page=30")
    Call<List<Category>> getCategories();

    @GET("products/categories/{id}")
    Call<Category> getCategory(@Path("id") int id);

    // Review
    @GET("products/reviews/{id}")
    Call<ProductReview> getReview(@Path("id") int id);

    @GET("products/reviews?per_page=30")
    Call<List<ProductReview>> getReviews();
}
