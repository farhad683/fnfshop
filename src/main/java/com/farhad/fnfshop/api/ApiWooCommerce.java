package com.farhad.fnfshop.api;

import me.gilo.woodroid.Woocommerce;

public class ApiWooCommerce {
    public static Woocommerce sWoocommerce=null;
    public static final String BASE_URL="https://www.fnf-shop.com/wp-json/wc/v3/";
    public static  Woocommerce getApiClient(){
        if(sWoocommerce == null){
            sWoocommerce = Woocommerce.Builder()
//                    .setSiteUrl("https://www.fnf-shop.com/")
                    .setSiteUrl("https://www.fnf-shop.com")
                    .setApiVersion(Woocommerce.API_V3)
//                    .setConsumerKey("ck_4ff858c97e1ed097cc24cb95593b2bcc45e835d5")
                    .setConsumerKey("ck_fa8db11b74faa51aaccbaf098a10fd062e223af3")
//                    .setConsumerSecret("cs_24865d302c443dec4a1391e5d2a438ba46e95cb6")
                    .setConsumerSecret("cs_68967d70f10adf64b53458dfc82cc5ee688f1c3b")
                    .build();
        }
        return sWoocommerce;
    }
}
