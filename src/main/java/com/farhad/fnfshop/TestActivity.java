package com.farhad.fnfshop;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.farhad.fnfshop.api.ApiClient;
import com.farhad.fnfshop.api.ApiServices;
import com.farhad.fnfshop.api.ApiWooCommerce;
import com.squareup.picasso.Picasso;

import java.util.List;

import me.gilo.woodroid.models.Category;
import me.gilo.woodroid.models.Product;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestActivity extends AppCompatActivity {
    private static final String TAG = "TAG";
    private ApiServices apiServices;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
//        TextView tv = findViewById(R.id.tv);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.INTERNET},1);
        }
        ImageView iv = findViewById(R.id.iv);
        apiServices= ApiClient.getInstance().create(ApiServices.class);
        /*ApiWooCommerce.getApiClient().CategoryRepository().categories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                Log.d(TAG, "onResponse: " + response.body().size());
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.d(TAG, "onResponse: " + t.getMessage());
            }
        });*/
        apiServices.getCategories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                for (int i = 0; i < response.body().size(); i++){
                    try {
                        String src = response.body().get(i).getImage().getSrc();
//                    String src = response.body().get(0).getImage().getName();
                        if(src != null){
                            Log.d(TAG, "onResponse: " + src);
                            Picasso.get().load(src).fit().centerInside().into(iv);

                        } else {
                            Log.d(TAG, "onResponse: image not set");
                        }
                    } catch (Exception e){
                        Log.d(TAG, "onResponse: on catch" + e.getMessage());

                    }
                }



            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.d(TAG, "onResponse: " + t.getMessage());

            }
        });

//        Call<List<Product>> call = apiServices.getProducts();
        /*call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.d(TAG, "onResponse: " + response.body());
                Toast.makeText(TestActivity.this, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });*/
        /*ApiWooCommerce.getApiClient().ProductRepository().products().enqueue(new Callback<List<me.gilo.woodroid.models.Product>>() {
            @Override
            public void onResponse(Call<List<me.gilo.woodroid.models.Product>> call, Response<List<me.gilo.woodroid.models.Product>> response) {
                Log.d(TAG, "onResponse: " + response.body().get(0).getName());

//                tv.setText(response.body().get(10).getName());
            }

            @Override
            public void onFailure(Call<List<me.gilo.woodroid.models.Product>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });*/
//        getCategories();
//        getSingleCategory(3);
//        createProductList();
//        Picasso.get().load("https://www.fnf-shop.com/wp-content/uploads/2018/02/shop22_product14.jpg").fit().centerInside().into(iv);

        // It works
        /*ApiWooCommerce.getApiClient().ProductRepository().products().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
//                Log.d(TAG, "onResponse: " + response.body().size());
                Log.d(TAG, "onResponse: " + response.body().get(0).getName());

            }

            @Override
            public void onFailure(Call<List<me.gilo.woodroid.models.Product>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });*/
    }

    private void getSingleCategory(int i) {
        ApiWooCommerce.getApiClient().CategoryRepository().category(i).enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                Log.d(TAG, "onResponse: " + response.body().getName());
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
    private void createProductList() {
//        mProductList.add(new ProductItem(R.drawable.product1, "Digital Camera", 1200.0));
        /*mProductList.add(new ProductItem(R.drawable.category2, "Sound System", 500));
        mProductList.add(new ProductItem(R.drawable.product1, "Digital Camera", 1200));
        mProductList.add(new ProductItem(R.drawable.category2, "Sound System", 500));*/

        apiServices.getProducts().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Log.d(TAG, "onResponse: " + response.body().size());
            }
            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Log.d(TAG, "onResponse: " + t.getMessage());
            }
        });
    }

    private void getCategories() {
        ApiWooCommerce.getApiClient().CategoryRepository().categories().enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                Log.d(TAG, "onResponse: Cat size: " + response.body().get(0).getId());
                Log.d(TAG, "onResponse: Cat size: " + response.body().get(0).getName());
                Log.d(TAG, "onResponse: Cat size: " + response.body().size());
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.d(TAG, "onFailure: cat");
            }
        });
    }
}


